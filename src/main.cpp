#include <exception>
#include <cstdlib>
#include <glrenderer.h>
#include <nwi/nwi.h>
#include <logger/logger.h>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define WINDOW_TITLE "cosmos fog"
#define MAX_FPS 24

const double FRAME_DURATION = 1.0 / MAX_FPS;

static void framebufferResizeCallback(NwiWindow* nwiWindow, int a_width, int a_height)
{
    auto pRenderer = reinterpret_cast<std::unique_ptr<Renderer>*>(nwiWindow->userData());
    pRenderer->get()->resizeSurface(a_width, a_height);
}

int main()
{
    if (!nwiInit())
    {
        Log::d("NWI init error");
        return EXIT_FAILURE;
    }

    NwiWindow* window;

    NwiWindowCreationInfo windowInfo{};
    windowInfo.width = WINDOW_WIDTH;
    windowInfo.height = WINDOW_HEIGHT;
    windowInfo.title = WINDOW_TITLE;

    window = nwiCreateWindow(windowInfo);

    if (!window)
    {
        nwiTerminate();
        Log::d("window init error");
        return EXIT_FAILURE;
    }

    nwiMakeContextCurrent(window);

    int width = 0, height = 0;

    window->getFramebufferSize(width, height);

    std::unique_ptr<Renderer> renderer = gl::initRenderer(width, height);

    if (!renderer)
    {
        nwiDestroyWindow(window);
        nwiTerminate();
        return EXIT_FAILURE;
    }

    window->setUserData(&renderer);

    window->setFramebufferSizeCallback(framebufferResizeCallback);

    double startTime = nwiGetTime();

    double lastFrameTime = 0.0;

    while (!window->shouldClose())
    {
        nwiPollEvents();
        double time = nwiGetTime();

        if (time - lastFrameTime >= FRAME_DURATION)
        {
            lastFrameTime = time;
            renderer->drawFrame(time - startTime);
            window->swapBuffers();
        }
    }
    nwiDestroyWindow(window);
    nwiTerminate();
    renderer.release();
    return EXIT_SUCCESS;
}
