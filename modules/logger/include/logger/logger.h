#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <iostream>
#include <logger_export.h>

namespace Log
{
	template<typename... Args>
	std::string strf(const char* fmt, Args&&... args)
	{
        auto length = std::snprintf(nullptr, 0, fmt, args...) + 1;
		auto buf = new char[length + 1];
		std::snprintf(buf, length, fmt, args...);
		std::string result(buf);
		delete[] buf;
        return result;
	}

	void LOGGER_EXPORT d(std::string arg);
}

#endif // LOGGER_H
