cmake_minimum_required(VERSION 3.20)

include(GenerateExportHeader)

project(logger)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_BUILD_TYPE RELEASE)

set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

file(GLOB SRC "${SRC_DIR}/*.c" "${SRC_DIR}/*.cpp")
file(GLOB_RECURSE SRC_INCLUDE "${INCLUDE_DIR}/*.h" "${INCLUDE_DIR}/*.hpp")

add_library(${PROJECT_NAME} SHARED ${SRC} ${SRC_INCLUDE})
add_library(module::${PROJECT_NAME} ALIAS ${PROJECT_NAME})

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME ${CMAKE_BINARY_DIR}/exports/${PROJECT_NAME}/${PROJECT_NAME}_export.h)
target_include_directories(${PROJECT_NAME} PUBLIC ${INCLUDE_DIR} ${CMAKE_BINARY_DIR}/exports/${PROJECT_NAME})
