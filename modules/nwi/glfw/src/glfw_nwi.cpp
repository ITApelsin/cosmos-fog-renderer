#include <nwi/nwi.h>
#include <GLFW/glfw3.h>

struct GlfwNwiWindowWrapper : public NwiWindow
{
    GLFWwindow* window;
    NwiFramebuffersizefun frambufferSizeCallback = 0;
    void* pUserData = 0;

    GlfwNwiWindowWrapper(GLFWwindow* a_window) : window(a_window) { }
    ~GlfwNwiWindowWrapper() override;

    void getFramebufferSize(int& out_width, int& out_height) override;

    void swapBuffers() override;

    void* userData() override;
    void setUserData(void* userData) override;
    
    void setFramebufferSizeCallback(NwiFramebuffersizefun fun) override;

    bool shouldClose() override;
};

bool nwiInit() 
{
    return glfwInit();
}

void nwiTerminate()
{
    glfwTerminate();
}

NwiWindow* nwiCreateWindow(NwiWindowCreationInfo& createInfo)
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    auto glfwWindow = glfwCreateWindow(createInfo.width, createInfo.height, createInfo.title, 0, 0);
    glfwMakeContextCurrent(glfwWindow);

    if (!glfwWindow)
    {
        return nullptr;
    }

    GlfwNwiWindowWrapper* nwiWindow = new GlfwNwiWindowWrapper(glfwWindow);

    glfwSetWindowUserPointer(glfwWindow, nwiWindow);

    return nwiWindow;
}

void nwiDestroyWindow(NwiWindow* nwiWindow)
{
    delete reinterpret_cast<GlfwNwiWindowWrapper*>(nwiWindow);
}

void nwiPollEvents()
{
    glfwPollEvents();
}

void nwiMakeContextCurrent(NwiWindow* window)
{
    glfwMakeContextCurrent(reinterpret_cast<GlfwNwiWindowWrapper*>(window)->window);
}

static void glfwFramebufferSizeCallback(GLFWwindow* a_window, int a_width, int a_height)
{
    auto glfwUserData = glfwGetWindowUserPointer(a_window);
    auto nwiWindowWrapper = reinterpret_cast<GlfwNwiWindowWrapper*>(glfwUserData);
    nwiWindowWrapper->frambufferSizeCallback(nwiWindowWrapper, a_width, a_height);
}

GlfwNwiWindowWrapper::~GlfwNwiWindowWrapper()
{
    glfwDestroyWindow(window);
}

void GlfwNwiWindowWrapper::getFramebufferSize(int& out_width, int& out_height)
{
    glfwGetFramebufferSize(window, &out_width, &out_height);
}

void GlfwNwiWindowWrapper::swapBuffers()
{
    glfwSwapBuffers(window);
}

void GlfwNwiWindowWrapper::setUserData(void* userData)
{
    this->pUserData = userData;
}

void GlfwNwiWindowWrapper::setFramebufferSizeCallback(NwiFramebuffersizefun fun)
{
    this->frambufferSizeCallback = fun;
    glfwSetFramebufferSizeCallback(this->window, fun ? glfwFramebufferSizeCallback : 0);
}

void* GlfwNwiWindowWrapper::userData()
{
    return this->pUserData;
}

bool GlfwNwiWindowWrapper::shouldClose()
{
    return glfwWindowShouldClose(window);
}


double nwiGetTime()
{
    return glfwGetTime();
}

