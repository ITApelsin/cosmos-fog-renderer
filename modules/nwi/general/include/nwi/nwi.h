#ifndef NWI_H
#define NWI_H

struct NwiWindow;

typedef void(* NwiFramebuffersizefun) (NwiWindow*, int, int);

struct NwiWindowCreationInfo 
{
    int width;
    int height;
    const char* title;
};

struct NwiWindow
{
    virtual ~NwiWindow() {};

    virtual void getFramebufferSize(int& out_width, int& out_height) = 0;
    virtual void swapBuffers() = 0;

    virtual void* userData() = 0;
    virtual void setUserData(void* userData) = 0;

    virtual void setFramebufferSizeCallback(NwiFramebuffersizefun fun) = 0;

    virtual bool shouldClose() = 0;
};

bool nwiInit();
void nwiTerminate();

NwiWindow* nwiCreateWindow(NwiWindowCreationInfo& creationInfo);

void nwiDestroyWindow(NwiWindow* window);

void nwiPollEvents();

void nwiMakeContextCurrent(NwiWindow* window);

double nwiGetTime();

#endif // NWI_H
