#ifndef GL_RENDERER_H
#define GL_RENDERER_H
#include <memory>
#include <renderer.h>

namespace gl
{
	std::unique_ptr<Renderer> initRenderer(int a_width, int a_height);
}

#endif // GL_RENDERER_H
