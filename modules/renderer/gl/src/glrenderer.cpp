#include <glad/glad.h>
#include <vector>
#include <cassert>
#include <logger/logger.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glrenderer.h>
#include "io_utils.h"

using namespace std;

struct LocationInfo
{
	GLint u_timeLoc;
	GLint u_resolutionLoc;
};

class GLRenderer : public Renderer
{
protected:
    GLuint m_program;
    LocationInfo m_locInfo;
	vector<GLuint> m_vboIds;
	GLuint m_vaoId;
public:
	GLRenderer(GLuint a_program, LocationInfo a_locInfo, vector<GLuint> a_vboIds, GLuint a_vaoId)
		: m_program(a_program), m_locInfo(a_locInfo), m_vboIds(a_vboIds), m_vaoId(a_vaoId) { }
	~GLRenderer()
	{
		glDeleteVertexArrays(1, &m_vaoId);
		glDeleteBuffers(m_vboIds.size(), m_vboIds.data());
		glDeleteProgram(m_program);
	}

	void drawFrame(float a_time) override
	{
		glUseProgram(m_program);
		glUniform1f(m_locInfo.u_timeLoc, a_time);
		glBindVertexArray(m_vaoId);
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

	void resizeSurface(int a_width, int a_height) override
	{
		glViewport(0, 0, a_width, a_height);
		glUseProgram(m_program);
		glUniform2f(m_locInfo.u_resolutionLoc, static_cast<float>(a_width), static_cast<float>(a_height));
	}
};

struct ShaderBuildInfo
{
    GLenum shaderType = 0;
	const char* sourcePath = nullptr;
};

struct ProgramBuildInfo
{
	int shaderCount;
	GLuint* shaders = nullptr;
};

static bool compileShaders(GLuint* destBuf, int count, ShaderBuildInfo* buildInfos)
{
	assert(destBuf && buildInfos);
	for (int i = 0; i < count; i++) 
	{
		ShaderBuildInfo& buildInfo = buildInfos[i];
		assert(buildInfo.sourcePath);

		string source_str;

		if (!io_utils::readFileAsString(buildInfo.sourcePath, source_str))
		{
			Log::d("can't read shader source");
			return false;
		}

		const char* source = source_str.c_str();

		GLuint shader = glCreateShader(buildInfo.shaderType);
		glShaderSource(shader, 1, &source, NULL);
		glCompileShader(shader);
		GLint isCompiled = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
			vector<char> infoLog(maxLength);
			glGetShaderInfoLog(shader, maxLength, &maxLength, infoLog.data());
			glDeleteShader(shader);
			Log::d(Log::strf("shader compile error:\n%s", infoLog.data()));
			
			for (int j = i - 1; j >= 0; j--)
			{
				glDeleteShader(destBuf[j]);
			}
			
			return false;
		}
		destBuf[i] = shader;
	}
	return true;
}

static bool linkPrograms(GLuint* destBuf, int count, ProgramBuildInfo* buildInfos) 
{
	assert(destBuf && buildInfos);
	for (int i = 0; i < count; i++)
	{
		ProgramBuildInfo& buildInfo = buildInfos[i];
		GLuint program = glCreateProgram();
		for (int i = 0; i < buildInfo.shaderCount; i++)
		{
			glAttachShader(program, buildInfo.shaders[i]);
		}
		glLinkProgram(program);
		for (int i = 0; i < buildInfo.shaderCount; i++)
		{
			glDetachShader(program, buildInfo.shaders[i]);
		}
		GLint isLinked = 0;
		vector<char> infoLog;
		glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
		if (isLinked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
			infoLog.resize(maxLength);
			glGetProgramInfoLog(program, maxLength, &maxLength, infoLog.data());
			Log::d(Log::strf("link program error:\n%s", infoLog.data()));
			glDeleteProgram(program);

			for (int j = i - 1; j >= 0; j--)
			{
				glDeleteProgram(destBuf[j]);
			}

			return false;
		}
		destBuf[i] = program;
	}
	return true;
}

unique_ptr<Renderer> gl::initRenderer(int a_width, int a_height)
{
    if (!gladLoadGL())
    {
        Log::d("glad load error");
        return nullptr;
    }

	ShaderBuildInfo shaderBuildInfos[2];
	shaderBuildInfos[0].shaderType = GL_VERTEX_SHADER;
	shaderBuildInfos[0].sourcePath = "shaders/cosmos-fog-sh.vert";

	shaderBuildInfos[1].shaderType = GL_FRAGMENT_SHADER;
	shaderBuildInfos[1].sourcePath = "shaders/cosmos-fog-sh.frag";

	vector<GLuint> shaders(2);

	if (!compileShaders(shaders.data(), shaders.size(), shaderBuildInfos))
	{
		return nullptr;
	}

	ProgramBuildInfo programBuildInfo{};
	programBuildInfo.shaderCount = shaders.size();
	programBuildInfo.shaders = shaders.data();

	GLuint program;
	bool linkSuccess = linkPrograms(&program, 1, &programBuildInfo);
	for (auto shader : shaders)
	{
		glDeleteShader(shader);
	}
	if (!linkSuccess)
	{
		return nullptr;
	}

	LocationInfo locInfo{};
	locInfo.u_resolutionLoc = glGetUniformLocation(program, "resolution");
	locInfo.u_timeLoc = glGetUniformLocation(program, "time");

	assert(locInfo.u_timeLoc != -1 && locInfo.u_resolutionLoc != -1);

	glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

	const glm::vec2 vertises[] = {
		{1.0f,	1.0f},	// top right
		{1.0f,	-1.0f},	// bottom right
		{-1.0f,	-1.0f},	// bottom left
		{-1.0f,	1.0f}	// top left
	};

	const GLuint indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	GLuint vaoId;
	vector<GLuint> vboIds(2);

	glGenVertexArrays(1, &vaoId);
	glGenBuffers(vboIds.size(), vboIds.data());

	glBindVertexArray(vaoId);

	glBindBuffer(GL_ARRAY_BUFFER, vboIds[0]);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec2), vertises, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIds[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), reinterpret_cast<GLvoid*>(0));
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	auto renderer = make_unique<GLRenderer>(program, locInfo, vboIds, vaoId);

	renderer->resizeSurface(a_width, a_height);

	return renderer;
}
