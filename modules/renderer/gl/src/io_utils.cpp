#include "io_utils.h"
#include <fstream>
#include <sstream>
#include <exception>
#include <vector>
#include <string>
#include <logger/logger.h>

using namespace std;

bool io_utils::readFileAsString(const char* a_path, std::string& out_dest) 
{
	stringstream strs;
	fstream file(a_path);
	if (!file.is_open())
	{
		Log::d(Log::strf("can't open file: %s", a_path));
		return false;
	}

	string line;
	while (getline(file, line))
	{
		strs << line << '\n';
	}
	file.close();
	out_dest = strs.str().c_str();
	return true;
}