#ifndef SHADER_UTILS_H
#define SHADER_UTILS_H

#include <string>
#include <glad/glad.h>

namespace io_utils
{
	bool readFileAsString(const char* a_path, std::string& out_dest);

	bool compileShaderFromSource(GLenum a_shaderType, const char* a_source, GLuint& out_dest);
}
 
#endif // SHADER_UTILS_H