#ifndef RENDERER_H
#define RENDERER_H

#include <glm/glm.hpp>

class Renderer
{
public:
	virtual void drawFrame(float a_time) = 0;
	virtual void resizeSurface(int a_width, int a_height) = 0;
};

#endif // RENDERER_H
